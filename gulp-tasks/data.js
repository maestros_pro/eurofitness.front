
module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;

	return ()=> gulp.src([`${args.PATH.DATA.SOURCE}*`])
		.pipe(gulp.dest(args.PATH.DATA.PUBLIC));
};
