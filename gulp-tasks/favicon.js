
module.exports = function (args) {

	let gulp = args.gulp, $ = args.$, PATH = args.PATH, fs = args.fs,
	options = {
		siteName: 'siteName',
		iconsPath: '/img/favicon/'
	};


	if ( fs.existsSync(PATH.FAVICON.DATA) ){

		$.util.log($.util.colors.cyan('favicon update'));

		let currentVersion = JSON.parse(fs.readFileSync(PATH.FAVICON.DATA)).version;
		return ()=> {
			$.realFavicon.checkForUpdates(currentVersion, function(err) { if (err) { throw err; } });
		}
	} else {
		return (done)=> {$.realFavicon.generateFavicon({
			masterPicture: PATH.FAVICON.ICON,
			dest: PATH.FAVICON.SOURCE,
			iconsPath: options.iconsPath,
			design: {
				ios: {
					pictureAspect: 'backgroundAndMargin',
					backgroundColor: '#ffffff',
					margin: '28%',
					assets: {
						ios6AndPriorIcons: false,
						ios7AndLaterIcons: false,
						precomposedIcons: false,
						declareOnlyDefaultIcon: true
					}
				},
				desktopBrowser: {},
				windows: {
					pictureAspect: 'whiteSilhouette',
					backgroundColor: '#2b5797',
					onConflict: 'override',
					assets: {
						windows80Ie10Tile: false,
						windows10Ie11EdgeTiles: {
							small: false,
							medium: true,
							big: false,
							rectangle: false
						}
					}
				},
				androidChrome: {
					pictureAspect: 'shadow',
					themeColor: '#ffffff',
					manifest: {
						name: options.siteName,
						display: 'standalone',
						orientation: 'notSet',
						onConflict: 'override',
						declared: true
					},
					assets: {
						legacyIcon: false,
						lowResolutionIcons: false
					}
				},
				safariPinnedTab: {
					pictureAspect: 'silhouette',
					themeColor: '#5bbad5'
				}
			},
			settings: {
				scalingAlgorithm: 'Mitchell',
				errorOnImageTooSmall: false
			},
			markupFile: PATH.FAVICON.DATA
		}, function() {
			$.util.log($.util.colors.green('favicon make, siteName: ' + options.siteName));

			gulp.src([`${PATH.FAVICON.SOURCE}*`])
				.pipe(gulp.dest(PATH.FAVICON.PUBLIC));

			done();
		});
		}
	}
};